package com.pxa.hr.data.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
/**
 * Represents an employee result object as would be returned by the API.
 * Note that here objects will contain a list of reports.
 */
public class EmployeeResult {
    @NotNull
    private int id;
    @NotNull
    private String name;
    private String title;
    private List<Integer> reports;
}
