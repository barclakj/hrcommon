package com.pxa.hr.data;

import com.pxa.hr.data.model.Employee;

import java.util.List;

public interface EmployeeDatasource {
    void deleteEmployee(final int id);
    void addEmployee(final int id, final String name, final String title, final int reportsTo);
    Employee getEmployee(final int id);
    List<Employee> listEmployeeReports(final int id);
}
