package test.pxa.hr.data;

import com.pxa.hr.data.EmployeeDatasource;
import com.pxa.hr.data.InMemoryEmployeeDatasource;
import com.pxa.hr.data.model.Employee;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

public class EmployeeModelTests {

    private EmployeeDatasource datasource = new InMemoryEmployeeDatasource();

    @Test
    public void canAddAndFetchEmployee() {
        datasource.addEmployee(1, "John Smith", "CEO", 0);
        Employee emp = datasource.getEmployee(1);
        Assertions.assertNotNull(emp);
        Assertions.assertEquals(emp.getName(), "John Smith");
        Assertions.assertEquals(emp.getTitle(), "CEO");
    }

    @Test
    public void canAddEmployeeAndReports() {
        datasource.addEmployee(1, "John Smith", "CEO", 0);
        datasource.addEmployee(2, "Employee 2", "T2", 1);
        datasource.addEmployee(3, "Employee 3", "T3", 1);
        datasource.addEmployee(6, "Employee 6", "T6", 1);
        datasource.addEmployee(100, "Henderson", "T100", 2);

        List<Integer> reportees = datasource.listEmployeeReports(1).stream()
                .map(employee -> {
                    return employee.getId();
                })
                .collect(Collectors.toList());
        Employee emp = datasource.getEmployee(1);
        Assertions.assertTrue(reportees.contains(2));
        Assertions.assertTrue(reportees.contains(3));
        Assertions.assertTrue(reportees.contains(6));
    }

    @Test
    public void canDeleteEmployee() {
        int id = (int)(Math.random()*100.);
        datasource.addEmployee(id, "Some name", "some role", 0);
        Employee emp = datasource.getEmployee(id);
        Assertions.assertNotNull(emp);
        datasource.deleteEmployee(id);
        Assertions.assertNull(datasource.getEmployee(id));
    }

    @Test
    public void canListEmployeeReports() {
        datasource.addEmployee(1, "John Smith", "CEO", 0);
        datasource.addEmployee(2, "Employee 2", "T2", 1);
        datasource.addEmployee(3, "Employee 3", "T3",1);
        datasource.addEmployee(6, "Employee 6", "T6", 1);

        List<Employee> employeeList = datasource.listEmployeeReports(1);
        Assertions.assertNotNull(employeeList);
        Assertions.assertTrue(employeeList.size()==3);
    }

}
