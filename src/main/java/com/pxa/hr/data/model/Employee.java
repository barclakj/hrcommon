package com.pxa.hr.data.model;


import lombok.*;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
/**
 * Represents an Employee record as it may be in a database.
 * Note that the reports relationship is from child to parent here.
 */
public class Employee {
    @NotNull
    private int id;
    @NotNull
    private String name;
    private String title;
    private int reportsTo;
}
