package com.pxa.hr.data;

import com.pxa.hr.data.model.Employee;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This is a brutally simple implementation of datasource where
 * objects are simply stored in memory in a hashmap.
 */
public class InMemoryEmployeeDatasource implements EmployeeDatasource {
    private Map<Integer, Employee> allEmployees = new HashMap<Integer, Employee>();

    public void deleteEmployee(int id) {
        allEmployees.remove(id);
    }

    public void addEmployee(int id, String name, String title, int reportsTo) {
        deleteEmployee(id); // remove in case employee id already in use
        Employee e = Employee.builder().id(id)
                .name(name)
                .title(title)
                .reportsTo(reportsTo)
                .build();
        allEmployees.put(id, e);
    }

    public Employee getEmployee(int id) {
        return allEmployees.get(id);
    }

    public List<Employee> listEmployeeReports(int id) {
        return allEmployees.values()
                .stream()
                .filter(emp -> emp.getReportsTo()== id)
                .collect(Collectors.toList());
    }
}
